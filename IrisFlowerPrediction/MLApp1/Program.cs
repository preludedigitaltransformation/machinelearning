﻿using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;
using System;

namespace myApp
{
    class Program
    {
        // STEP 1: Define your data structures
        // IrisData is used to provide training data, and as
        // input for prediction operations
        // - First 4 properties are inputs/features used to predict the label
        // - Label is what you are predicting, and is only set when training
        public class IrisData
        {
            [LoadColumn(0)]
            public float SepalLength;

            [LoadColumn(1)]
            public float SepalWidth;

            [LoadColumn(2)]
            public float PetalLength;

            [LoadColumn(3)]
            public float PetalWidth;

            [LoadColumn(4)]
            public string Label;
        }

        // IrisPrediction is the result returned from prediction operations
        public class IrisPrediction
        {
            [ColumnName("PredictedLabel")]
            public string PredictedLabels;
        }

        static void Main(string[] args)
        {
            try
            {            
            // STEP 2: Create a ML.NET environment  
            MLContext mlContext = new MLContext();

            // If working in Visual Studio, make sure the 'Copy to Output Directory'
            // property of iris-data.txt is set to 'Copy always'
            IDataView trainingDataView = mlContext.Data.ReadFromTextFile<IrisData>(path: "iris-data.txt", hasHeader: false, separatorChar: ',');

            // STEP 3: Transform your data and add a learner
            // Assign numeric values to text in the "Label" column, because only
            // numbers can be processed during model training.
            // Add a learning algorithm to the pipeline. e.g.(What type of iris is this?)
            // Convert the Label back into original text (after converting to number in step 3)
            var pipeline = mlContext.Transforms.Conversion.MapValueToKey("Label")
                .Append(mlContext.Transforms.Concatenate("Features", "SepalLength", "SepalWidth", "PetalLength", "PetalWidth"))
                .AppendCacheCheckpoint(mlContext)
                .Append(mlContext.MulticlassClassification.Trainers.StochasticDualCoordinateAscent(labelColumn: "Label", featureColumn: "Features"))
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

            // STEP 4: Train your model based on the data set  
            var model = pipeline.Fit(trainingDataView);

            // STEP 5: Use your model to make a prediction
            // You can change these numbers to test different predictions
            Single sLen, sWid, pLen, pWid;
            Console.WriteLine("Enter Sepal Length:");
            sLen = Single.Parse(Console.ReadLine());
            Console.WriteLine("Enter Sepal Width:");
            sWid = Single.Parse(Console.ReadLine());
            Console.WriteLine("Enter Petal Length:");
            pLen = Single.Parse(Console.ReadLine());
            Console.WriteLine("Enter Petal Width:");
            pWid = Single.Parse(Console.ReadLine());
            Console.WriteLine("Press enter to know the result..");

            char cmd;

            while ((cmd = Console.ReadKey().KeyChar) !=  'q')
            {
                if (cmd == 's')
                {
                    Console.WriteLine("");
                    Console.WriteLine("Enter Sepal Length:");
                    sLen = Single.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Sepal Width:");
                    sWid = Single.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Petal Length:");
                    pLen = Single.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Petal Width:");
                    pWid = Single.Parse(Console.ReadLine());
                }
                var prediction = model.CreatePredictionEngine<IrisData, IrisPrediction>(mlContext).Predict(
                    new IrisData()
                    {
                        SepalLength = sLen,
                        SepalWidth = sWid,
                        PetalLength = pLen,
                        PetalWidth = pWid,
                    });

                Console.WriteLine("");
                Console.WriteLine($"Predicted flower type is: {prediction.PredictedLabels}");
                Console.WriteLine("");
                Console.WriteLine("Press q to exit.... or s to startover... Enter for result");
            }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}